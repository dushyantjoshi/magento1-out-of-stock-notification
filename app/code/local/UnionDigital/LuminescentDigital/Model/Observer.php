<?php

class UnionDigital_LuminescentDigital_Model_Observer {

    const OUTOFSTOCKNOTIFICATION_MAIL_TEMPLATE = 'outofstock_notification';

    public function checkOutOfStockSendNotifs($observer) {
        /**
         * @var $items array array($productId => array('qty'=>$qty, 'item'=>$stockItem))
         */
        $stockItem = $observer->getEvent()->getItem();
        $product = Mage::getModel('catalog/product')->load((int) $stockItem->getProductId());      

        Mage::log($stockItem->getIsInStock(), null, 'cataloginventory_stock_item_save_after.log');
        if (!$stockItem->getIsInStock()) {

            $subscriptions = explode(',', $product->getNotificationAddress());            
            if (count($subscriptions) > 0) {
                
                $prodUrl = Mage::getBaseUrl();
                $prodUrl = str_replace("/index.php", "", $prodUrl);
                $prodUrl = $prodUrl . $product->getData('url_path');

                
                $productUrl = $product->getResource()->getAttribute('url_key')->getFrontend()->getValue($product);
                        
                $storeId = Mage::app()->getStore()->getId();
                
                $emailTemplate = self::OUTOFSTOCKNOTIFICATION_MAIL_TEMPLATE;

                $translate = Mage::getSingleton('core/translate');

                foreach ($subscriptions as $subscription) {

                    $translate->setTranslateInline(false);
                    Mage::getModel('core/email_template')
                            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
                            ->sendTransactional(
                                    $emailTemplate, 'support', $subscription, '', array(
                                'product_name' => $product->getName(),
                                'product_url' => $productUrl,
                    ));
                    $translate->setTranslateInline(true);
                }
            }
        }
    }

}
