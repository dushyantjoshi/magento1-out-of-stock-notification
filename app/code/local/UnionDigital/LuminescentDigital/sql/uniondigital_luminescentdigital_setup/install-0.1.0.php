<?php
 
$installer = $this;
 
$installer->startSetup();

$installer->addAttribute('catalog_product', 'notification_address', array(
    'group' => 'General',
    'label' => 'Notification Addresses (separated By Comma)',
    'type' => 'text',
    'input' => 'text',
    'backend' => '',
    'frontend' => '',
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => true,
    'visible_in_advanced_search' => false,
    'unique' => false,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'frontend_class'  => 'validate-comma-separated-emails'
));


$installer->endSetup();
