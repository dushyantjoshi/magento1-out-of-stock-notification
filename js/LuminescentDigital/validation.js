if ('Validation' in window) {
    Validation.addAllThese([
        ['validate-comma-separated-emails', 'Please enter a valid email address.', function (emaillist) {

                emaillist = emaillist.trim();
                if (emaillist.charAt(0) == ',' || emaillist.charAt(emaillist.length - 1) == ',') {
                    return false;
                }
                var emails = emaillist.split(',');
                var invalidEmails = [];
                for (i = 0; i < emails.length; i++) {
                    var v = emails[i].trim();
                    if (!Validation.get('validate-email').test(v)) {
                        invalidEmails.push(v);
                    }
                }
                if (invalidEmails.length) {
                    return false;
                }
                return true;

            }]
    ]);
}